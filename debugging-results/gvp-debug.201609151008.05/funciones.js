var funciones_version = "8.1.3";

function parserJson(json,key)
{  
	try
	{	
		var obj = JSON.parse(json);
		var response = obj[key];
		return response;
	}
	catch(err)
	{
		 return "Error Parser Json";	
	}	
}

function getJson(json)
{   
 try 
 {
	 var jsonString = JSON.stringify(json);
	 return jsonString;
 }
 catch(err)
 {
	 return "Error Get Json";	
 }
}