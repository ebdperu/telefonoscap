<%@ Page Language="C#"  ContentType="application/json;charset=UTF-8" Inherits="BackEndlogic" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="log4net" %>
<%@ Import Namespace="Newtonsoft.Json.Linq"%>
<%@ Import Namespace="Newtonsoft.Json.Converters"%>
<%@ Import Namespace="Newtonsoft.Json" %>


<script runat="server">
    
    public override JObject PerformLogic(JObject state, Hashtable additionalParams)
    {

       // variables de entrada		
		string numero = (string) additionalParams["NUMERO"];		
		string ws = (string) additionalParams["WS"];
		string sURL = ws + "?telefono=" + numero;
  			
		JObject result = new JObject();
		
		// variables      	    
		string sConexion = "";
		string responseFromServer = "";
		HttpWebResponse response = null;
        
		try
		{
            // Create a request for the URL. 		
            WebRequest request = WebRequest.Create(sURL);
            // If required by the server, set the credentials.
            request.Credentials = CredentialCache.DefaultCredentials;
            // Get the response.
            response = (HttpWebResponse)request.GetResponse();
            sConexion = Convert.ToString((int) response.StatusCode);
		}
        catch (WebException e) 
        {
        	if (e.Status == WebExceptionStatus.ProtocolError)
            {
                response = (HttpWebResponse) e.Response;
		  		sConexion = Convert.ToString((int) response.StatusCode);
            }
        }
        
		// Get the stream containing content returned by the server.
		Stream dataStream = response.GetResponseStream();
		// Open the stream using a StreamReader for easy access.
		StreamReader reader = new StreamReader(dataStream);
		// Read the content.
		responseFromServer = reader.ReadToEnd();           
		// Cleanup the streams and the response.
		reader.Close();
		dataStream.Close();
		response.Close();  
		
		JObject objJson = JObject.Parse(responseFromServer);
  		
  		//return
		result.Add(new JProperty("RESPONSE", objJson));
		result.Add(new JProperty("CONEXION", sConexion)); 
        
        return result;
    }

    // If the passState parameter of the backend logic block is set to true.
    // The state variable contains all variables from the voice application.
    // If the passState parameter is set to false, the state variable will be null.

    // Example of how to access data from the state object.
    // Note that "Input1" is the name of the Input block and 
    // "Var1" is the name of the user-defined variable.
    //************************************
    //String userInput = state["Input1"];
    // String userVariable = state["Var1"];
    //************************************
    // The additionalParams hashtable contains any additional input parameters passed
    // in the backend logic block.

    // Example:
    // Note that "Param1" is the parameter name of the input parameter.
    //*************************************************
    // String param = additionalParams.get["Param1"];
    //*************************************************

    // Finally, this method must return a result object.  Any values stored in 
    // this result object will be reassigned to output parameters defined in the voice application.
    // For example, if the voice application declares an output parameter called "OutParam",
    // the output parameter will get the value "Value".
    //*******************************************************
    //result = new JObject(new JProperty("OutParam", value));
    
    //For example to set multiple values 
    // result = new JObject(new JProperty("Key1", "Value1"),
    //new JProperty("Key2", "Value2"),
    // new JProperty("Key3","Value3"));
    //*******************************************************

</script>