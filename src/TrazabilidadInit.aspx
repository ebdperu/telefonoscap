<%@ Page Language="C#"  ContentType="application/json;charset=UTF-8" Inherits="BackEndlogic" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="log4net" %>
<%@ Import Namespace="Newtonsoft.Json.Linq"%>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="System.Xml.Linq" %>


<script runat="server">
    
    public override JObject PerformLogic(JObject state, Hashtable additionalParams)
    {

   		JObject result = new JObject();
   		Random r = new Random();
		//string vConnID = (string) additionalParams["vConnID"];
		string vConnID = Convert.ToString(r.Next(10000, 1000000));
		string numero = (string) additionalParams["NUMERO"];
   		string xmlFilePath = "C:/Windows/Temp/calls/" + vConnID + ".xml";
       
       	XmlTextWriter writer = new XmlTextWriter(xmlFilePath, System.Text.Encoding.UTF8);
        writer.WriteStartDocument(true);
        writer.Formatting = System.Xml.Formatting.Indented;
        writer.Indentation = 2;
        writer.WriteStartElement("Call");

        writer.WriteStartElement("Information");
        writer.WriteElementString("grupoAtencion","");
		writer.WriteElementString("tipoContactoCliente","");
		writer.WriteElementString("telefono","");
		writer.WriteElementString("imei","");
		writer.WriteElementString("custCode","");
		writer.WriteElementString("solucion","");
		writer.WriteElementString("cantidadUnidades","");
		writer.WriteElementString("tamanoCuenta","");
		writer.WriteElementString("estadoLinea","");
		writer.WriteElementString("numeroContrato","");
		writer.WriteElementString("idPlantTarifario","");
		writer.WriteElementString("idCliente","");
		writer.WriteElementString("tecnologia","");
		writer.WriteElementString("rentaBasica","");
		writer.WriteElementString("tipoMercado","");
		writer.WriteElementString("tipoServicio","");
		writer.WriteElementString("tipoDocumento","");
		writer.WriteElementString("numeroDocumento","");
		writer.WriteElementString("segmento","");
		writer.WriteElementString("nombreCliente","");
		writer.WriteElementString("idClienteCRM","");
		writer.WriteElementString("estadoContrato","");
		writer.WriteElementString("descripcionEstadoContrato","");
		writer.WriteElementString("razonLineaDesactivada","");
		writer.WriteElementString("informacionAdicionalBloqueo","");
		writer.WriteElementString("ivr","5");
		writer.WriteElementString("ANI",numero);
		writer.WriteElementString("Date", Convert.ToString(DateTime.Now));
        writer.WriteEndElement();
        
        writer.WriteStartElement("Options");
        writer.WriteEndElement();

        writer.WriteEndDocument();
        writer.Close();

        //*****************************
        //* Add your custom code here 
        //*****************************
        
        
        result.Add(new JProperty("vConnID", vConnID));
        
        return result;
    }

    // If the passState parameter of the backend logic block is set to true.
    // The state variable contains all variables from the voice application.
    // If the passState parameter is set to false, the state variable will be null.

    // Example of how to access data from the state object.
    // Note that "Input1" is the name of the Input block and 
    // "Var1" is the name of the user-defined variable.
    //************************************
    //String userInput = state["Input1"];
    // String userVariable = state["Var1"];
    //************************************
    // The additionalParams hashtable contains any additional input parameters passed
    // in the backend logic block.

    // Example:
    // Note that "Param1" is the parameter name of the input parameter.
    //*************************************************
    // String param = additionalParams.get["Param1"];
    //*************************************************

    // Finally, this method must return a result object.  Any values stored in 
    // this result object will be reassigned to output parameters defined in the voice application.
    // For example, if the voice application declares an output parameter called "OutParam",
    // the output parameter will get the value "Value".
    //*******************************************************
    //result = new JObject(new JProperty("OutParam", value));
    
    //For example to set multiple values 
    // result = new JObject(new JProperty("Key1", "Value1"),
    //new JProperty("Key2", "Value2"),
    // new JProperty("Key3","Value3"));
    //*******************************************************

</script>