<%@ Page Language="C#"  ContentType="application/json;charset=UTF-8" Inherits="BackEndlogic" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="log4net" %>
<%@ Import Namespace="Newtonsoft.Json.Linq"%>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="System.Xml.Linq" %>


<script runat="server">
    
    public override JObject PerformLogic(JObject state, Hashtable additionalParams)
    {

		string vConnID = (string) additionalParams["vConnID"];
		string vRegistro = (string) additionalParams["vRegistro"];
		JObject registro = JObject.Parse(vRegistro);
		string proceso = "";
		
		if (registro["idProceso"].ToString().Equals("0")){
			proceso = registro["proceso"].ToString();
		}
		else {
			proceso = registro["proceso"]["proceso"].ToString();
		}
		
       	JObject result = new JObject();
       	
       	string xmlFilePath = "C:/Windows/Temp/calls/" + vConnID + ".xml";
       	//string xmlFilePath = "C:/Windows/Temp/calls/" + "12345" + ".xml";
       
       	XDocument doc = XDocument.Load(xmlFilePath);
		XElement call = doc.Element("Call");
		XElement options = call.Element("Options");
		
		options.Add(new XElement("Option", 
					new XElement("idProgramacion", registro["idProgramacion"].ToString()),
					new XElement("idMenu", registro["idMenu"].ToString()),
					new XElement("menu", registro["menu"].ToString()),
					new XElement("idMenuPadre", registro["idMenuPadre"].ToString()),
					new XElement("idAudio", registro["idAudio"].ToString()),
					new XElement("idAudioOpcion", registro["idAudioOpcion"].ToString()),
					new XElement("opcionMenu", registro["opcionMenu"].ToString()),
					new XElement("tipoRegistro", registro["tipoRegistro"].ToString()),
					new XElement("idProceso", registro["idProceso"].ToString()),
					new XElement("proceso", proceso), 
					new XElement("idAudioTx", registro["idAudioTx"].ToString()),
					new XElement("idGrupoTx", registro["idGrupoTx"].ToString()),
					new XElement("grupoTransferencia", registro["grupoTransferencia"].ToString()),
					new XElement("subMenu", registro["subMenu"].ToString()),
					new XElement("estado",registro["estado"].ToString()),
					new XElement("idEmp",registro["idEmp"].ToString()),
					new XElement("idIvr",registro["idIvr"].ToString()),
					new XElement("error",""),
					new XElement("Date", Convert.ToString(DateTime.Now))));
		doc.Save(xmlFilePath);
        
        return result;
    }

    // If the passState parameter of the backend logic block is set to true.
    // The state variable contains all variables from the voice application.
    // If the passState parameter is set to false, the state variable will be null.

    // Example of how to access data from the state object.
    // Note that "Input1" is the name of the Input block and 
    // "Var1" is the name of the user-defined variable.
    //************************************
    //String userInput = state["Input1"];
    // String userVariable = state["Var1"];
    //************************************
    // The additionalParams hashtable contains any additional input parameters passed
    // in the backend logic block.

    // Example:
    // Note that "Param1" is the parameter name of the input parameter.
    //*************************************************
    // String param = additionalParams.get["Param1"];
    //*************************************************

    // Finally, this method must return a result object.  Any values stored in 
    // this result object will be reassigned to output parameters defined in the voice application.
    // For example, if the voice application declares an output parameter called "OutParam",
    // the output parameter will get the value "Value".
    //*******************************************************
    //result = new JObject(new JProperty("OutParam", value));
    
    //For example to set multiple values 
    // result = new JObject(new JProperty("Key1", "Value1"),
    //new JProperty("Key2", "Value2"),
    // new JProperty("Key3","Value3"));
    //*******************************************************

</script>