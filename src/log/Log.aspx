<%@ Page Language="C#"  ValidateRequest="false" ContentType="application/json;charset=UTF-8" Inherits="BackEndlogic" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="log4net" %>
<%@ Import Namespace="Newtonsoft.Json.Linq"%>
<%@ Import Namespace="Newtonsoft.Json.Converters"%>
<%@ Import Namespace="Newtonsoft.Json" %>

<script runat="server">
    
    static readonly ILog m_Log = LogManager.GetLogger("debugPrueba");      
  
    public void EscribeLog(string sEscribeLogs)
    {
       log4net.Config.XmlConfigurator.Configure(); 
	   m_Log.Debug("STRING: " + sEscribeLogs );   
    }
           
              
    public override JObject PerformLogic(JObject state, Hashtable additionalParams)
	{	  
		string sParametros = (string) additionalParams["PARAMETROS"];		
		
		EscribeLog("************pruebaaa**************"+sParametros);                                         
   	      			
      	//example parse	
      	JObject result = new JObject();        		
	    result.Add(new JProperty("Response", "OK"));
	    return result; 	       		  
	}
    
</script>