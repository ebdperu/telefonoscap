<%@ Page Language="C#"  ValidateRequest="false" ContentType="application/json;charset=UTF-8" Inherits="BackEndlogic" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="log4net" %>
<%@ Import Namespace="Newtonsoft.Json.Linq"%>
<%@ Import Namespace="Newtonsoft.Json.Converters"%>
<%@ Import Namespace="Newtonsoft.Json" %>

<script runat="server">
    
	public override JObject PerformLogic(JObject state, Hashtable additionalParams)
	{		
		// variables de entrada		
		string numero = (string) additionalParams["NUMERO"];
		string ws = (string) additionalParams["WS"];
		string sURL = ws + "?telefono=" + numero + "&idIvr=4";
  			
		JObject result = new JObject();
		
		// variables      	    
		string sConexion = "";
		string responseFromServer = "";
		HttpWebResponse response = null;
        
		try
		{
            // Create a request for the URL. 		
            WebRequest request = WebRequest.Create(sURL);
            // If required by the server, set the credentials.
            request.Credentials = CredentialCache.DefaultCredentials;
            // Get the response.
            response = (HttpWebResponse)request.GetResponse();
            sConexion = Convert.ToString((int) response.StatusCode);
		}
        catch (WebException e) 
        {
        	if (e.Status == WebExceptionStatus.ProtocolError)
            {
                response = (HttpWebResponse) e.Response;
		  		sConexion = Convert.ToString((int) response.StatusCode);
            }
        }
        
		// Get the stream containing content returned by the server.
		Stream dataStream = response.GetResponseStream();
		// Open the stream using a StreamReader for easy access.
		StreamReader reader = new StreamReader(dataStream);
		// Read the content.
		responseFromServer = reader.ReadToEnd();           
		// Cleanup the streams and the response.
		reader.Close();
		dataStream.Close();
		response.Close();  
		
		JObject objJson = JObject.Parse(responseFromServer);
  		
  		//return
		result.Add(new JProperty("CONEXION", sConexion)); 
		result.Add(new JProperty("RESPONSE", objJson));
        
        return result;
		
	}

</script>