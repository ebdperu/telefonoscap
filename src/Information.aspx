<%@ Page Language="C#"  ContentType="application/json;charset=UTF-8" Inherits="BackEndlogic" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="log4net" %>
<%@ Import Namespace="Newtonsoft.Json.Linq"%>
<%@ Import Namespace="Newtonsoft.Json" %>


<script runat="server">
    
    public override JObject PerformLogic(JObject state, Hashtable additionalParams)
    {

   		JObject result = new JObject();
		string vConnID = (string) additionalParams["vConnID"];
		//string vConnID = "102060";
		string vResponse = (string) additionalParams["vResponse"];
		JObject response = JObject.Parse(vResponse);
   		string xmlFilePath = "C:/Windows/Temp/calls/" + vConnID + ".xml";
   		
   		XmlDocument xdoc = new XmlDocument();
   		xdoc.Load(xmlFilePath);
   		
   		//xdoc.SelectSingleNode("/Call/Information/grupoAtencion").InnerText = "My Location";
		xdoc.SelectSingleNode("/Call/Information/grupoAtencion").InnerText = response["grupoAtencion"].ToString();
		xdoc.SelectSingleNode("/Call/Information/tipoContactoCliente").InnerText = response["tipoContactoCliente"].ToString();
		xdoc.SelectSingleNode("/Call/Information/telefono").InnerText = response["telefono"].ToString();
		xdoc.SelectSingleNode("/Call/Information/imei").InnerText = response["imei"].ToString();
		xdoc.SelectSingleNode("/Call/Information/custCode").InnerText = response["custCode"].ToString();
		xdoc.SelectSingleNode("/Call/Information/solucion").InnerText = response["solucion"].ToString();
		xdoc.SelectSingleNode("/Call/Information/cantidadUnidades").InnerText = response["cantidadUnidades"].ToString();
		xdoc.SelectSingleNode("/Call/Information/tamanoCuenta").InnerText = response["tamanoCuenta"].ToString();
		xdoc.SelectSingleNode("/Call/Information/estadoLinea").InnerText = response["estadoLinea"].ToString();
		xdoc.SelectSingleNode("/Call/Information/numeroContrato").InnerText = response["numeroContrato"].ToString();
		xdoc.SelectSingleNode("/Call/Information/idPlantTarifario").InnerText = response["idPlantTarifario"].ToString();
		xdoc.SelectSingleNode("/Call/Information/idCliente").InnerText = response["idCliente"].ToString();
		xdoc.SelectSingleNode("/Call/Information/tecnologia").InnerText = response["tecnologia"].ToString();
		xdoc.SelectSingleNode("/Call/Information/rentaBasica").InnerText = response["rentaBasica"].ToString();
		xdoc.SelectSingleNode("/Call/Information/tipoMercado").InnerText = response["tipoMercado"].ToString();
		xdoc.SelectSingleNode("/Call/Information/tipoServicio").InnerText = response["tipoServicio"].ToString();
		xdoc.SelectSingleNode("/Call/Information/tipoDocumento").InnerText = response["tipoDocumento"].ToString();
		xdoc.SelectSingleNode("/Call/Information/numeroDocumento").InnerText = response["numeroDocumento"].ToString();
		xdoc.SelectSingleNode("/Call/Information/segmento").InnerText = response["segmento"].ToString();
		xdoc.SelectSingleNode("/Call/Information/nombreCliente").InnerText = response["nombreCliente"].ToString();
		xdoc.SelectSingleNode("/Call/Information/idClienteCRM").InnerText = response["idClienteCRM"].ToString();
		xdoc.SelectSingleNode("/Call/Information/estadoContrato").InnerText = response["estadoContrato"].ToString();
		xdoc.SelectSingleNode("/Call/Information/descripcionEstadoContrato").InnerText = response["descripcionEstadoContrato"].ToString();
		xdoc.SelectSingleNode("/Call/Information/razonLineaDesactivada").InnerText = response["razonLineaDesactivada"].ToString();
		xdoc.SelectSingleNode("/Call/Information/informacionAdicionalBloqueo").InnerText = response["informacionAdicionalBloqueo"].ToString();
				
		xdoc.Save(xmlFilePath);
   		

       //*****************************
        //* Add your custom code here 
        //*****************************
        
        
        return result;
    }

    // If the passState parameter of the backend logic block is set to true.
    // The state variable contains all variables from the voice application.
    // If the passState parameter is set to false, the state variable will be null.

    // Example of how to access data from the state object.
    // Note that "Input1" is the name of the Input block and 
    // "Var1" is the name of the user-defined variable.
    //************************************
    //String userInput = state["Input1"];
    // String userVariable = state["Var1"];
    //************************************
    // The additionalParams hashtable contains any additional input parameters passed
    // in the backend logic block.

    // Example:
    // Note that "Param1" is the parameter name of the input parameter.
    //*************************************************
    // String param = additionalParams.get["Param1"];
    //*************************************************

    // Finally, this method must return a result object.  Any values stored in 
    // this result object will be reassigned to output parameters defined in the voice application.
    // For example, if the voice application declares an output parameter called "OutParam",
    // the output parameter will get the value "Value".
    //*******************************************************
    //result = new JObject(new JProperty("OutParam", value));
    
    //For example to set multiple values 
    // result = new JObject(new JProperty("Key1", "Value1"),
    //new JProperty("Key2", "Value2"),
    // new JProperty("Key3","Value3"));
    //*******************************************************

</script>