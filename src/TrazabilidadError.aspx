<%@ Page Language="C#"  ContentType="application/json;charset=UTF-8" Inherits="BackEndlogic" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="log4net" %>
<%@ Import Namespace="Newtonsoft.Json.Linq"%>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="System.Xml.Linq" %>


<script runat="server">
    
    public override JObject PerformLogic(JObject state, Hashtable additionalParams)
    {

       	JObject result = new JObject();

       	string vConnID = (string) additionalParams["vConnID"];
		string texto = (string) additionalParams["TEXTO"];
       	string xmlFilePath = "C:/Windows/Temp/calls/" + vConnID + ".xml";
       	//string xmlFilePath = "C:/Windows/Temp/calls/" + "12345" + ".xml";
       
       	XDocument doc = XDocument.Load(xmlFilePath);
		XElement call = doc.Element("Call");
		XElement options = call.Element("Options");
		
		//options.Add(new XElement("Error", 
		//			new XElement("Mensaje", texto),
		//			new XElement("Date", Convert.ToString(DateTime.Now))));
					
		options.Add(new XElement("Option", 
					new XElement("idProgramacion", ""),
					new XElement("idMenu", ""),
					new XElement("menu", ""),
					new XElement("idMenuPadre", ""),
					new XElement("idAudio", ""),
					new XElement("idAudioOpcion", ""),
					new XElement("opcionMenu", ""),
					new XElement("tipoRegistro", ""),
					new XElement("idProceso", ""),
					new XElement("proceso", ""), 
					new XElement("idAudioTx", ""),
					new XElement("idGrupoTx", ""),
					new XElement("grupoTransferencia", ""),
					new XElement("subMenu", ""),
					new XElement("estado",""),
					new XElement("idEmp",""),
					new XElement("idIvr",""),
					new XElement("error",texto),
					new XElement("Date", Convert.ToString(DateTime.Now))));
		doc.Save(xmlFilePath);        
        
        return result;
    }

    // If the passState parameter of the backend logic block is set to true.
    // The state variable contains all variables from the voice application.
    // If the passState parameter is set to false, the state variable will be null.

    // Example of how to access data from the state object.
    // Note that "Input1" is the name of the Input block and 
    // "Var1" is the name of the user-defined variable.
    //************************************
    //String userInput = state["Input1"];
    // String userVariable = state["Var1"];
    //************************************
    // The additionalParams hashtable contains any additional input parameters passed
    // in the backend logic block.

    // Example:
    // Note that "Param1" is the parameter name of the input parameter.
    //*************************************************
    // String param = additionalParams.get["Param1"];
    //*************************************************

    // Finally, this method must return a result object.  Any values stored in 
    // this result object will be reassigned to output parameters defined in the voice application.
    // For example, if the voice application declares an output parameter called "OutParam",
    // the output parameter will get the value "Value".
    //*******************************************************
    //result = new JObject(new JProperty("OutParam", value));
    
    //For example to set multiple values 
    // result = new JObject(new JProperty("Key1", "Value1"),
    //new JProperty("Key2", "Value2"),
    // new JProperty("Key3","Value3"));
    //*******************************************************

</script>